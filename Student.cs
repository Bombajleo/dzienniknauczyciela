﻿namespace Dziennik
{
    public class Student
    {
        public int NotebookNumber;
        public int UniqueId;

        public string Name;
        public string LastName;

        public string PESEL;
        public string MatherName;

        public string FatherName;
        public string Phone;

      




        public Student(int notebookNumber, string name, string lastname)
        {
            this.NotebookNumber = notebookNumber;
            this.Name = name;
            this.LastName = lastname;

        }

        public Student (int notebookNumber, string name, string lastname, string PESEL)

        {
            this.NotebookNumber = notebookNumber;
            this.Name = name;
            this.LastName = lastname;
            this.PESEL = PESEL;
        }
        public Student(int notebookNumber, string name, string lastname, string PESEL, string Mathername)
        {
            this.NotebookNumber = notebookNumber;
            this.Name = name;
            this.LastName = lastname;
            this.PESEL = PESEL;
            this.MatherName = Mathername;


        }
        public Student(int notebookNumber, string name, string lastname, string PESEL, string Mathername, string Fathername)
        {
            this.NotebookNumber = notebookNumber;
            this.Name = name;
            this.LastName = lastname;
            this.PESEL = PESEL;
            this.MatherName = Mathername;
            this.FatherName = Fathername;
            
        }

        public Student(int notebookNumber, string name, string lastname, string PESEL, string Mathername, string Fathername, int Uniqueid)
        {
            this.NotebookNumber = notebookNumber;
            this.Name = name;
            this.LastName = lastname;
            this.PESEL = PESEL;
            this.MatherName = Mathername;
            this.FatherName = Fathername;
            this.UniqueId = Uniqueid;
        }
        public Student(int notebookNumber, string name, string lastname, string PESEL, string Mathername, string Fathername, int Uniqueid, string phone)
        {
            this.NotebookNumber = notebookNumber;
            this.Name = name;
            this.LastName = lastname;
            this.PESEL = PESEL;
            this.MatherName = Mathername;
            this.FatherName = Fathername;
            this.UniqueId = Uniqueid;
            this.Phone = phone;
        }

        
    }
}